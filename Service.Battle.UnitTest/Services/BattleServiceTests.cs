﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Dapr.Client;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service.Battle.Models;
using Service.Battle.Models.Monster;
using Service.Battle.Services;
using Service.Battle.Storage;
using Service.Battle.Utils.Profiles;

namespace Service.Battle.UnitTest.Services
{
    [TestClass]
    public class BattleServiceTests
    {
        private readonly Mapper _autoMapper =
            new(new MapperConfiguration(cfg => cfg.AddProfile<BattleMappingProfile>()));

        private readonly Fixture _fixture = new();
        private readonly Mock<DaprClient> _mockDaprClient;
        private readonly AttackService _attackService;

        private readonly Mock<IMonsterClient> _mockMonsterClient;


        public BattleServiceTests()
        {
            MockRepository mockRepository = new(MockBehavior.Strict);
            _mockMonsterClient = mockRepository.Create<IMonsterClient>();
            _mockDaprClient = mockRepository.Create<DaprClient>();
            _attackService = new AttackService(new NullLogger<AttackService>());
        }


        [TestMethod]
        public async Task StartBattleSuccess()
        {
            var teamA = _fixture.Create<Team>();
            var teamB = _fixture.Create<Team>();

            var mockBattle = _fixture.Create<Models.Battle>();
            var battle = mockBattle with
            {
                Teams = new[]
                {
                    teamA, teamB,
                },
            };
            var sut = CreateBattleService();

            var monsterTeamAList = new List<Monster>();
            // 3 mosnters on both sides
            for (var i = 0; i < 3; i++)
            {
                monsterTeamAList.Add(TestData.CreateMonster(i,
                    0));
            }

            var monsterTeamBList = new List<Monster>();
            // 3 mosnters on both sides
            for (var i = 0; i < 3; i++)
            {
                monsterTeamBList.Add(TestData.CreateMonster(i, 1));
            }

            teamA.Monsters = monsterTeamAList;
            teamB.Monsters = monsterTeamBList;

            _mockDaprClient.Setup(client => client.PublishEventAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<BattleStartedEvent>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            var result = await sut.StartBattle(battle);

            result.Active.Should().NotBeNull();
            result.Active.TurnQueue.Should().HaveCount(6);
        }

        [TestMethod]
        public async Task EndBattleSuccess()
        {
            var teamA = _fixture.Create<Team>();
            var teamB = _fixture.Create<Team>();

            var mockBattle = _fixture.Create<Models.Battle>();
            var battle = mockBattle with
            {
                Teams = new[]
                {
                    teamA, teamB,
                },
            };
            var sut = CreateBattleService();

            var monsterTeamAList = new List<Monster>();
            // 3 monsters on both sides
            for (var i = 0; i < 3; i++)
            {
                var mon = TestData.CreateMonster(i, 0);
                mon.BattleValues=mon.BattleValues with{RemainingHp = 10};
                monsterTeamAList.Add(mon);
            }

            var monsterTeamBList = new List<Monster>();
            // 3 monsters on both sides
            for (var i = 3; i < 6; i++)
            {
                var mon = TestData.CreateMonster(i, 1);
                mon.BattleValues = mon.BattleValues with
                {
                    RemainingHp = i == 3 ? 0 : 10
                };
                monsterTeamBList.Add(mon);
            }

            teamA.Monsters = monsterTeamAList;
            teamB.Monsters = monsterTeamBList;

            battle.Active = new ActiveBattle
            {
                TurnQueue = new List<int>
                {
                    0, 1, 2,
                },
                Id = 0,
                TurnCount = 0,
            };


            _mockMonsterClient.Setup(client => client.UpdateMonster(It.IsAny<Monster>()))
                .ReturnsAsync(_fixture.Create<Monster>());

            _mockDaprClient.Setup(client => client.PublishEventAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<BattleTurnQueueEvent>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            _mockDaprClient.Setup(client => client.PublishEventAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.Is<BattleEndedEvent>(e => e.Winner.Id == 0),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            var result = await sut.ExecuteAttackCommand(battle, new AttackCommand
            {
                Source = 0, Target = 3,
            });
        }

        [TestMethod]
        public async Task NextTurnSuccess()
        {
            var teamA = _fixture.Create<Team>();
            var teamB = _fixture.Create<Team>();

            var mockBattle = _fixture.Create<Models.Battle>();
            var battle = mockBattle with
            {
                Teams = new[]
                {
                    teamA, teamB,
                },
            };
            var sut = CreateBattleService();

            var monsterTeamAList = new List<Monster>();
            // 3 monsters on both sides
            for (var i = 0; i < 3; i++)
            {
                var mon = TestData.CreateMonster(i, 0);
                monsterTeamAList.Add(mon);
            }

            var monsterTeamBList = new List<Monster>();
            // 3 monsters on both sides
            for (var i = 3; i < 6; i++)
            {
                var mon = TestData.CreateMonster(i, 1);
                monsterTeamBList.Add(mon);
            }

            teamA.Monsters = monsterTeamAList;
            teamB.Monsters = monsterTeamBList;

            battle.Active = new ActiveBattle
            {
                TurnQueue = new List<int>
                {
                    0, 1, 2,
                },
                Id = 0,
                TurnCount = 0,
            };


            _mockMonsterClient.Setup(client => client.UpdateMonster(It.IsAny<Monster>()))
                .ReturnsAsync(_fixture.Create<Monster>());

            _mockDaprClient.Setup(client => client.PublishEventAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<BattleTurnQueueEvent>(),
                    It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            var result = await sut.ExecuteAttackCommand(battle, new AttackCommand
            {
                Source = 0, Target = 3,
            });

            Assert.IsNotNull(result.Active);
            result.Active.TurnQueue.Should().HaveCount(2);
            result.Active.TurnQueue.Should().BeEquivalentTo(new List<int>
            {
                1, 2,
            });
            teamB.Monsters.Single(m => m.Id == 3).BattleValues.RemainingHp.Should().BeLessThan(100);
        }


        private BattleService CreateBattleService()
        {
            return new(new NullLogger<BattleService>(),
                _autoMapper,
                _mockMonsterClient.Object,
                _mockDaprClient.Object,
                _attackService);
        }
    }
}
