using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Models.Monster;
using Service.Battle.Services;
using Service.Battle.Storage;
using Service.Battle.Utils.Profiles;

namespace Service.Battle.UnitTest.Services
{
    [TestClass]
    public class DailyBattleServiceTests
    {
        private readonly Fixture _fixture = new();

        private readonly Mock<IDailyBattleStorage> _dailyBattleStorageMock;
        private readonly Mock<IBattleStorage> _battleStorageMock;
        private readonly Mock<IUserClient> _userClientMock;
        private readonly Mock<IMonsterClient> _monsterClientMock;
        private readonly Mock<IMonsterService> _monsterServiceMock;

        private readonly Mapper _autoMapper =
            new(new MapperConfiguration(cfg => cfg.AddProfile<BattleMappingProfile>()));


        public DailyBattleServiceTests()
        {
            var repository = new MockRepository(MockBehavior.Strict);
            _dailyBattleStorageMock = repository.Create<IDailyBattleStorage>();
            _battleStorageMock = repository.Create<IBattleStorage>();
            _userClientMock = repository.Create<IUserClient>();
            _monsterClientMock = repository.Create<IMonsterClient>();
            _monsterServiceMock = repository.Create<IMonsterService>();
        }


        [TestMethod]
        public async Task ResetSuccess()
        {
            _dailyBattleStorageMock.Setup(x => x.FindByUserId(It.IsAny<int>(), It.Is<bool>(v => v == true)))
                .ReturnsAsync(new List<BattleEntity>());

            _userClientMock.Setup(x => x.CreateUser(It.IsAny<CreateUserRequest>()))
                .ReturnsAsync(_fixture.Create<User>());

            _monsterClientMock.Setup(x => x.CreateMonster(It.IsAny<Monster>()))
                .ReturnsAsync(_fixture.Create<Monster>());
            _monsterServiceMock.Setup(x => x.CreateDefaultMonster(It.IsAny<int>()))
                .Returns(_fixture.Create<Monster>());
            _battleStorageMock.Setup(x => x.AddUserIfNotExists(It.IsAny<UserEntity>()))
                .Returns(Task.CompletedTask);

            _battleStorageMock.Setup(x => x.AddMonsters(It.IsAny<IEnumerable<MonsterEntity>>()))
                .Returns(Task.CompletedTask);

            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));
            _battleStorageMock.Setup(x => x.Add(It.IsAny<BattleEntity>()))
                .ReturnsAsync(_fixture.Create<BattleEntity>());

            var sut = CreateService();
            var newDailyBattles = await sut.Reset(1);

            newDailyBattles.Should().HaveCount(3);
        }


        private DailyBattleService CreateService()
        {
            return new DailyBattleService(_dailyBattleStorageMock.Object, _battleStorageMock.Object,
                _monsterClientMock.Object, _userClientMock.Object, _monsterServiceMock.Object, _autoMapper);
        }
    }
}
