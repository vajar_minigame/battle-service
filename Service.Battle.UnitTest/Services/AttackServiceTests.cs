﻿using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.Battle.Services;

namespace Service.Battle.UnitTest.Services
{
    [TestClass]
    public class AttackServiceTests
    {
        [TestMethod]
        public void AttackSuccess()
        {
            var source = TestData.CreateMonster(1, 0);
            var target = TestData.CreateMonster(2, 1);
            var sut = CreateAttackService();
            var (newSource, newTarget) = sut.Attack(source.BattleValues, target.BattleValues);

            newTarget.RemainingHp.Should().Be(90);
            newSource.Should().BeEquivalentTo(source.BattleValues);
        }

        [TestMethod]
        public void AttackLessThanZero()
        {
            var source = TestData.CreateBattleValues();
            var target = TestData.CreateBattleValues() with
            {
                RemainingHp = 1
            };
            var sut = CreateAttackService();
            var (newSource, newTarget) = sut.Attack(source, target);

            newTarget.RemainingHp.Should().Be(0);
            newSource.Should().BeEquivalentTo(source);
        }

        private AttackService CreateAttackService()
        {
            return new(new NullLogger<AttackService>());
        }
    }
}
