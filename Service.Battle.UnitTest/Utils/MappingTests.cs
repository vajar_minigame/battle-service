﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.Battle.Utils.Profiles;

namespace Service.Battle.UnitTest.Utils
{
    [TestClass]
    public class MappingTests
    {
        private readonly Mapper _autoMapper;

        public MappingTests()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<BattleMappingProfile>());
            mapperConfiguration.AssertConfigurationIsValid();
            _autoMapper = new Mapper(mapperConfiguration);
        }

        [TestMethod]
        public void DummyMethod()
        {
            
        }
    }
}
