using System;
using Service.Battle.Models.Monster;

namespace Service.Battle.UnitTest
{
    public static class TestData
    {
        public static Monster CreateMonster(int id, int userId)
        {
            return new()
            {
                Id = id,
                Name = $"Monster{id}",
                BattleValues = CreateBattleValues(),
                IsTest = false,
                UserId = userId,
                StatusValues = new StatusValues
                {
                    IsAlive = true,
                },
                LastUpdate = new DateTime(2020, 5, 5),
            };
        }

        public static BattleValues CreateBattleValues()
        {
            return new()
            {
                Attack = 10, Defense = 10, MaxHp = 100, RemainingHp = 100,
            };
        }
    }
}
