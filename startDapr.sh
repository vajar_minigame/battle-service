daprd --app-id service-battle \
    --app-port 5000 \
    --dapr-http-port 3504 \
    --dapr-grpc-port 50004 \
    --components-path ./Battle/components \
    --config ./tracing.yaml \
    --metrics-port 9090 \
    --placement-host-address "localhost:50005" \
    --log-level debug
