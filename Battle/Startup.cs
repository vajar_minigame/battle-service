using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Service.Battle.Actors;
using Service.Battle.Services;
using Service.Battle.Storage;
using Service.Battle.Utils.Profiles;
using UserClient = Service.Battle.Services.UserClient;

namespace Service.Battle
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddDapr();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // services.AddHostedService<AISystem>();
            services.AddScoped<UnitOfWork>();
            services.AddTransient<IMonsterClient, MonsterClientDapr>();
            services.AddTransient<IUserClient, UserClient>();
            services.AddTransient<IBattleService, BattleService>();
            services.AddTransient<IDailyBattleStorage, DailyBattleStorage>();
            services.AddTransient<IAttackService, AttackService>();
            services.AddDbContext<BattleDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("BattleDB")));
            //daprstuff
            services.AddDaprClient();
            services.AddActors(options =>
            {
                options.Actors.RegisterActor<BattleActor>();
            });

            services.AddAutoMapper(typeof(BattleMappingProfile));

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
            });
            services.AddOpenApiDocument();
            services.AddVersionedApiExplorer(options => options.SubstituteApiVersionInUrl = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCloudEvents();
            app.UseAuthorization();

            //app.UseReDoc(); // serve ReDoc UI

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapSubscribeHandler();
                endpoints.MapActorsHandlers();
            });
        }
    }
}
