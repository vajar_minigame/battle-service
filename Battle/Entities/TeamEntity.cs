﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Service.Battle.Entities
{
    public class TeamEntity
    {
        public int Id { get; set; }

        public IEnumerable<UserEntity> Users { get; set; } = new List<UserEntity>();
        
        public virtual IEnumerable<MonsterEntity> Monsters { get; set; } = new List<MonsterEntity>();

        [IgnoreDataMember]
        [JsonIgnore]
        public virtual BattleEntity BattleEntity { get; set; } = null!;
    }
}
