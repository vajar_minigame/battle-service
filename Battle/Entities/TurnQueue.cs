﻿using System.Collections.Generic;
using System.Linq;
using Service.Battle.Models;

namespace Service.Battle.Entities
{
    public static class TurnQueueExtension
    {
        public static IEnumerable<int> FilterDead(this IEnumerable<int> list,
            Models.Battle b)
        {
            var allMons = b.Teams.SelectMany(t => t.Monsters);
            return list.Select(mId => allMons.First(m => m.Id == mId))
                .Where(m => m.StatusValues.IsAlive)
                .Select(m => m.Id).ToList();
        }

        public static List<TurnQueueElement> RemoveElement(this List<TurnQueueElement> list, int index)
        {
            // everything after has to be reduced
            list.RemoveAt(index);
            list.ForEach(m =>
            {
                if (m.Priority > index)
                {
                    m.Priority -= 1;
                }
            });

            return list;
        }
    }
}
