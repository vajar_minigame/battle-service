﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Service.Battle.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        [IgnoreDataMember]
        [JsonIgnore]
        public virtual List<TeamEntity> TeamEntities { get; set; } = new();
    }
}
