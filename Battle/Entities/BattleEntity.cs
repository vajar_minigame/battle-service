﻿using System;
using System.Collections.Generic;
using Service.Battle.Models;

namespace Service.Battle.Entities
{
    public class BattleEntity
    {
        //todo ended battles are stored somehw
        public int Id { get; set; }
        public IList<TeamEntity> Teams { get; set; } = new List<TeamEntity>();
        public DateTime LastUpdate { get; set; }
        public BattleType Type { get; init; }
        public BattleState State { get; set; }
        public bool IsTest { get; set; }
    }
}
