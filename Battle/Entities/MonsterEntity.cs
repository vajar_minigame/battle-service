﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Service.Battle.Entities
{
    public class MonsterEntity
    {
        public int Id { get; set; }
        
        [IgnoreDataMember]
        [JsonIgnore]
        public virtual List<TeamEntity> TeamEntities { get; set; } = new();
    }
}
