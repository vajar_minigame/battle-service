﻿using System;

namespace Service.Battle.Entities
{
    public class EndedBattle
    {
        public int Id { get; set; }

        public DateTime EndDate { get; set; }
    }
}
