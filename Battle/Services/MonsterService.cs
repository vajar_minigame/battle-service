using Service.Battle.Models.Monster;

namespace Service.Battle.Services
{
    public class MonsterService : IMonsterService
    {
        public Monster CreateDefaultMonster(int userId)
        {
            return new Monster
            {
                Name = "rando",
                BattleValues = new BattleValues
                {
                    Attack = 10, Defense = 10, MaxHp = 100, RemainingHp = 100,
                },
                IsTest = false,
                UserId = userId,
            };
        }
    }
}
