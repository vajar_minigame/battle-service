using Service.Battle.Models.Monster;

namespace Service.Battle.Services
{
    public interface IMonsterService
    {
        Monster CreateDefaultMonster(int userId);
    }
}
