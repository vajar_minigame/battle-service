using System.Net.Http;
using System.Threading.Tasks;
using Dapr.Client;
using Service.Battle.Models;

namespace Service.Battle.Services
{
    public class UserClient : IUserClient
    {
        private readonly DaprClient _daprClient;

        public UserClient(DaprClient daprClient)
        {
            _daprClient = daprClient;
        }


        public async Task<User?> GetUserById(int id)
        {
            var response = await _daprClient.InvokeMethodAsync<User>(HttpMethod.Get, "service-user",
                $"/api/v1.0/user/{id}");
            return response;
        }

        public async Task<User> CreateUser(CreateUserRequest user)
        {
            var response = await _daprClient.InvokeMethodAsync<CreateUserRequest, User>(HttpMethod.Post, "service-user",
                "/api/v1.0/user", user);
            return response;
        }
    }
}
