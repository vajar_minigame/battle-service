using System;
using System.Net.Http;
using System.Threading.Tasks;
using Dapr.Client;
using Microsoft.Extensions.Logging;
using Service.Battle.Models.Monster;

namespace Service.Battle.Storage
{
    public interface IMonsterClient
    {
        Task<Monster?> GetMonById(int id);

        Task<Monster> UpdateMonster(Monster mon);

        Task<Monster> CreateMonster(Monster mon);
    }

    public class MonsterClientDapr : IMonsterClient
    {
        private readonly DaprClient _daprClient;
        private readonly ILogger<MonsterClientDapr> _logger;


        public MonsterClientDapr(ILogger<MonsterClientDapr> logger,
            DaprClient daprClient)
        {
            _logger = logger;

            _daprClient = daprClient;
        }

        public async Task<Monster?> GetMonById(int id)
        {
            var response = await _daprClient.InvokeMethodAsync<Monster>(HttpMethod.Get, "service-monster",
                $"/api/v1.0/monster/{id}");
            return response;
        }


        public async Task<Monster> UpdateMonster(Monster mon)
        {
            var updatedMon =
                await _daprClient.InvokeMethodAsync<Monster, Monster>(HttpMethod.Put, "service-monster",
                    $"api/v1.0/monster/{mon.Id}", mon);

            if (updatedMon == null)
            {
                _logger.LogInformation("bodyVal or battleVal are not set for mon with id {MonId}", mon.Id);
                throw new ArgumentException($"Could not update mon {mon.Id}");
            }

            return updatedMon;
        }
        

        public async Task<Monster> CreateMonster(Monster mon)
        {
            
            var createMonster = await _daprClient.InvokeMethodAsync<Monster, Monster>(HttpMethod.Post, "service-monster",
                "api/v1.0/monster", mon);
            return createMonster;
        }
    }
}
