﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using Dapr.Client;
using Microsoft.Extensions.Logging;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Models.Monster;
using Service.Battle.Storage;
using Service.Battle.Utils;

#endregion

namespace Service.Battle.Services
{
    public interface IBattleService
    {
        Task<Models.Battle> StartBattle(Models.Battle b);
        Task<Models.Battle> ExecuteAttackCommand(Models.Battle battle, AttackCommand attack);
    }

    public class BattleService : IBattleService
    {
        private const string PubSubName = "pubsub";
        private readonly DaprClient _daprClient;
        private readonly IAttackService _attackService;
        private readonly ILogger<BattleService> _logger;
        private readonly IMapper _mapper;
        private readonly IMonsterClient _monsterClient;

        public BattleService(ILogger<BattleService> logger,
            IMapper mapper,
            IMonsterClient monsterClient,
            DaprClient daprClient,
            IAttackService attackService)
        {
            _logger = logger;
            _mapper = mapper;
            _monsterClient = monsterClient;
            _daprClient = daprClient;
            _attackService = attackService;
        }

        public async Task<Models.Battle> ExecuteAttackCommand(Models.Battle battle, AttackCommand attack)
        {
            _logger.LogInformation("start attack command!");
            Guard.Against.Null(battle.Active, nameof(battle.Active));

            var source = GetMonsterById(battle, attack.Source);
            var target = GetMonsterById(battle, attack.Target);


            if (battle.Active.TurnQueue.First().Equals(source.Id) == false)
            {
                _logger.LogError("the chosen mon is not the next one; {NextMon} is next",
                    battle.Active.TurnQueue.First());
                throw new InvalidDataException("The chosen mon is not the next one");
            }

            var (newSourceBattleValues, newTargetBattleValues) =
                _attackService.Attack(source.BattleValues, target.BattleValues);
            source.BattleValues = newSourceBattleValues;
            target.BattleValues = newTargetBattleValues;
            UpdateMonStatus(target);
            UpdateMonStatus(source);

            await _monsterClient.UpdateMonster(target);
            var winningTeam = CheckVictory(battle);
            if (winningTeam != null)
            {
                await EndBattle(battle, winningTeam);
                return battle;
            }

            NextMon(battle);
            if (!battle.Active.TurnQueue.Any())
            {
                _logger.LogInformation("the next turn starts");
                StartNewTurn(battle);
            }

            await SendTurnQEvent(battle);
            battle.LastUpdate = DateTime.UtcNow;
            battle.Active.LastAction = DateTime.UtcNow;
            return battle;
        }

        public async Task<Models.Battle> StartBattle(Models.Battle b)
        {
            b.Active = new ActiveBattle
            {
                Id = b.Id,
                TurnCount = 0,
                StartTime = DateTime.UtcNow,
                LastAction = DateTime.UtcNow,
                TurnQueue = new List<int>(),
            };

            _logger.LogInformation("Send Event: {BattleStartedEvent}", JsonSerializer.Serialize(b));
            StartNewTurn(b);
            await SendStartBattleEvent(b);
            return b;
        }


        private static void UpdateMonStatus(Monster m)
        {
            if (m.BattleValues.RemainingHp > 0)
            {
                return;
            }

            m.StatusValues.IsAlive = false;
        }

        private static void StartNewTurn(Models.Battle b)
        {
            if (b.Active == null)
            {
                throw new Exception("active battle is not active");
            }

            var activeBattle = b.Active;
            activeBattle.TurnCount++;

            var monList = b.Teams.SelectMany(t => t.Monsters).Where(i => i.StatusValues.IsAlive).Select(i => i.Id)
                .ToList();


            activeBattle.TurnQueue = monList;

            if (activeBattle.TurnQueue.Count == 0)
            {
                throw new Exception("weird case where no player has a living mon anymore");
            }
        }

        private static Team? CheckVictory(Models.Battle b)
        {
            // win when only one team is alive

            var teamMons = b.Teams.Select(t => new
            {
                team = t, monsters = t.Monsters,
            });

            var aliveTeams =
                (from t in teamMons
                    where t.monsters.Any(m => m.StatusValues.IsAlive)
                    select t.team).ToList();

            if (aliveTeams.Count > 1)
            {
                return null;
            }

            return aliveTeams.Single();
        }


        private static Monster GetMonsterById(Models.Battle battle, int monId)
        {
            return battle.Teams.SelectMany(t => t.Monsters).First(m => m.Id == monId);
        }


        private async Task EndBattle(Models.Battle b, Team winningTeam)
        {
            await SendBattleEndedEvent(b, winningTeam);
        }

        private void NextMon(Models.Battle b)
        {
            if (b.Active == null)
            {
                throw new Exception("active battle is not active");
            }

            var activeBattle = b.Active;
            activeBattle.TurnQueue.RemoveAt(0);

            activeBattle.TurnQueue = activeBattle.TurnQueue.FilterDead(b).ToList();

            _logger.LogInformation("the number of mons in turnqueue is {TurnqueueCount}", activeBattle.TurnQueue.Count);
        }


        private async Task SendTurnQEvent(Models.Battle b)
        {
            Guard.Against.Null(b.Active, nameof(b.Active));
            var nextMon = b.Active.TurnQueue.First();
            var turnQEvent = new BattleTurnQueueEvent
            {
                Battle = b, BattleId = b.Id,
            };

            _logger.LogDebug("it's {NextMon} turn now", nextMon);
            await _daprClient.PublishEventAsync(PubSubName,
                $"{nameof(EventType.Battle)}/{nameof(BattleTurnQueueEvent)}",
                turnQEvent);
        }

        private async Task SendStartBattleEvent(Models.Battle b)
        {
            var battleStartedEvent = new BattleStartedEvent
            {
                Battle = b, BattleId = b.Id,
            };

            await _daprClient.PublishEventAsync(PubSubName, $"{nameof(EventType.Battle)}/{nameof(BattleStartedEvent)}",
                battleStartedEvent);
        }

        private async Task SendBattleEndedEvent(Models.Battle b, Team winningTeam)
        {
            var endEvent = new BattleEndedEvent
            {
                Battle = b, Winner = _mapper.Map<TeamEntity>(winningTeam), BattleId = b.Id,
            };

            _logger.LogInformation("Team {TeamId} won the battle", winningTeam.Id);
            await _daprClient.PublishEventAsync(PubSubName, $"{nameof(EventType.Battle)}/{nameof(BattleEndedEvent)}",
                endEvent);
        }
    }
}
