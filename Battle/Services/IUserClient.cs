using System.Threading.Tasks;
using Service.Battle.Models;

namespace Service.Battle.Services
{
    public interface IUserClient
    {
        Task<User?> GetUserById(int id);
        Task<User> CreateUser(CreateUserRequest user);
    }
}
