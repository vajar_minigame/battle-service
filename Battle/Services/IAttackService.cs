﻿using Service.Battle.Models.Monster;

namespace Service.Battle.Services
{
    public interface IAttackService
    {
        (BattleValues source,BattleValues target) Attack(BattleValues source, BattleValues target);
    }
}
