﻿using System;
using Microsoft.Extensions.Logging;
using Service.Battle.Models.Monster;

namespace Service.Battle.Services
{
    public class AttackService : IAttackService
    {
        private readonly ILogger<AttackService> _logger;

        public AttackService(ILogger<AttackService> logger)
        {
            _logger = logger;
        }

        public (BattleValues source, BattleValues target) Attack(BattleValues source, BattleValues target)
        {
            var effect = CalcAttackEffect(source);

            _logger.LogInformation("{Damage} damage was caused", effect.RemainingHp);

            var newTargetValues = AddValues(target, effect);
            return (source, newTargetValues);
        }

        private static BattleValues CalcAttackEffect(BattleValues source)
        {
            var effect = new BattleValues
            {
                RemainingHp = -source.Attack,
            };

            return effect;
        }

        private static BattleValues AddValues(BattleValues a, BattleValues b)
        {
            return new()
            {
                Attack = a.Attack + b.Attack,
                Defense = a.Defense + b.Defense,
                MaxHp = Math.Max(a.MaxHp + b.MaxHp, 0),
                RemainingHp = Math.Max(a.RemainingHp + b.RemainingHp, 0),
            };
        }
    }
}
