using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Storage;

namespace Service.Battle.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DailyBattleService
    {
        private readonly IDailyBattleStorage _dailyBattleStorage;
        private readonly IBattleStorage _battleStorage;
        private readonly IMonsterClient _monsterClient;
        private readonly IMonsterService _monsterService;
        private readonly IUserClient _userClient;
        private readonly IMapper _mapper;

        public DailyBattleService(IDailyBattleStorage dailyBattleStorage, IBattleStorage battleStorage,
            IMonsterClient monsterClient, IUserClient userClient, IMonsterService monsterService, IMapper mapper)
        {
            _dailyBattleStorage = dailyBattleStorage;
            _battleStorage = battleStorage;
            _monsterClient = monsterClient;
            _userClient = userClient;
            _monsterService = monsterService;
            _mapper = mapper;
        }

        public async Task<List<BattleEntity>> Reset(int userId)
        {
            var existingBattles = await GetDailyBattles(userId);
            foreach (var existingBattle in existingBattles)
            {
                await _battleStorage.Remove(existingBattle.Id);
            }

            var newBattles = new List<BattleEntity>();
            for (int i = 0; i < 3; i++)
            {
                var newBattle = await CreateNewBattle(userId);
                var newBattleEnt = await _battleStorage.Add(newBattle);
                newBattles.Add(newBattleEnt);
            }

            return newBattles;
        }
        
        public async Task<List<BattleEntity>> GetDailyBattles(int userId)
        {
            return await _dailyBattleStorage.FindByUserId(userId, true);
        }

        /// <summary>
        /// Create a new battle. For this a new enemy with monsters is created.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<BattleEntity> CreateNewBattle(int userId)
        {
            //create enemy
            var user = await _userClient.CreateUser(new CreateUserRequest { Username = "TestUser-battle" });

            await _battleStorage.AddUserIfNotExists(_mapper.Map<UserEntity>(user));

            var monsters = new List<MonsterEntity>();
            for (int i = 0; i < 3; i++)
            {
                var mon = _monsterService.CreateDefaultMonster(user.Id);
                var createdMonster = await _monsterClient.CreateMonster(mon);
                monsters.Add((_mapper.Map<MonsterEntity>(createdMonster)));
            }

            await _battleStorage.AddMonsters(monsters);

            var battleEntity = new BattleEntity
            {
                Teams = new List<TeamEntity>
                {
                    new() { Users = new List<UserEntity> { new() { Id = userId } } },
                    new() { Users = new List<UserEntity> { new() { Id = user.Id } }, Monsters = monsters }
                },
                IsTest = false,
                Type = BattleType.Daily,
            };

            var createdBattle = await _battleStorage.Add(battleEntity);
            return createdBattle;
        }

      
    }
}
