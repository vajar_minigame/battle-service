using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Service.Battle.Entities;

namespace Service.Battle.Configurations
{
    public class BattleEntityConfiguration : IEntityTypeConfiguration<BattleEntity>
    {
        public void Configure(EntityTypeBuilder<BattleEntity> builder)
        {
            builder.HasKey(t => t.Id);
        }
    }
}
