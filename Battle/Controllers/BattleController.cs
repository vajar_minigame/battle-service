using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapr.Actors;
using Dapr.Actors.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Service.Battle.Actors;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Models.Commands;
using Service.Battle.Storage;

namespace Service.Battle.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BattleController : ControllerBase
    {
        private readonly IBattleStorage _battleDb;
        private readonly BattleDbContext _dbBattleDbContext;
        private readonly IActorProxyFactory _factory;
        private readonly IMapper _mapper;
        private readonly IMonsterClient _monClient;


        public BattleController(ILogger<BattleController> logger,
            UnitOfWork unitOfWork,
            IMonsterClient monClient,
            IActorProxyFactory actorProxyFactory,
            IMapper mapper,
            BattleDbContext dbBattleDbContext)
        {
            _battleDb = unitOfWork.GetBattleRepo();
            _monClient = monClient;
            _mapper = mapper;
            _factory = actorProxyFactory;
            _dbBattleDbContext = dbBattleDbContext;
        }


        [HttpPost]
        public async Task<ActionResult<BattleView>> AddBattle(AddBattleRequest request)
        {
            var battle = request;

            var multiOccurrenceMonster = battle.Teams.SelectMany(t => t.Monsters)
                .GroupBy(m => m)
                .Where((i, _) => i.Count() > 1)
                .Select(grp => grp.Key);

            var multiOccurrenceUser = battle.Teams.SelectMany(t => t.Users)
                .GroupBy(m => m)
                .Where((i, _) => i.Count() > 1)
                .Select(grp => grp.Key);


            if (multiOccurrenceMonster.Count() > 1)
            {
                return BadRequest("mon is in 2 teams!");
            }


            if (multiOccurrenceUser.Count() > 1)
            {
                return BadRequest("user is in 2 teams!");
            }

            var overlap = false;
            foreach (var m in battle.Teams.SelectMany(t => t.Monsters))
            {
                if (await _battleDb.IsAlreadyInBattle(m))
                {
                    overlap = true;
                    break;
                }
            }

            if (overlap)
            {
                return BadRequest("a monster is already in an active battle");
            }
            //look that no other active or non ended battles exist with these mons

            var monstersTasks = battle.Teams.SelectMany(t => t.Monsters)
                .Select(async m => await _monClient.GetMonById(m));


            var monsterArray = await Task.WhenAll(monstersTasks);

            if (monsterArray.Any(m => m == null))
            {
                NotFound("could not find some monsters");
            }

            var mappedBattleEntity = new BattleEntity
            {
                Teams = _mapper.Map<List<TeamEntity>>(request.Teams),
            };
            //add mons teams and users
            //TODO use unit of work pattern!
            await _dbBattleDbContext.Monsters.AddRangeAsync(mappedBattleEntity.Teams.SelectMany(t => t.Monsters));
            await _dbBattleDbContext.Users.AddRangeAsync(mappedBattleEntity.Teams.SelectMany(t => t.Users));


            await _dbBattleDbContext.Teams.AddRangeAsync(mappedBattleEntity.Teams);

            var b = await _dbBattleDbContext.Battles.AddAsync(mappedBattleEntity);

            await _dbBattleDbContext.SaveChangesAsync();
            var battleView = _mapper.Map<BattleView>(b.Entity);
            return Ok(battleView);
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<BattleView>> GetBattleById(int id)
        {
            var battle = await _battleDb.GetBattleById(id);

            if (battle == null)
            {
                NotFound("the battle was not found");
            }

            var battleView = _mapper.Map<BattleView>(battle);

            return Ok(battleView);
        }

        [HttpGet("{id}/active")]
        public async Task<ActionResult<Models.Battle>> GetActiveBattleById(int id)
        {
            var bEntity = await _battleDb.GetBattleById(1);
            if (bEntity?.State != BattleState.Active)
            {
                return NotFound("the battle is not active");
            }

            var actorId = new ActorId($"{id}");
            var proxy = _factory.CreateActorProxy<IBattleActor>(actorId, nameof(BattleActor));

            var battle = await proxy.GetBattle();

            return Ok(battle);
        }


        [HttpGet]
        public async Task<ActionResult<List<BattleView>>> GetBattleByUserId([FromQuery] BattleQueryRequest request)
        {
            var battles = await _battleDb.GetBattlesByUserId(request.UserId.Value);

            var battleViews = battles.Select(b=>_mapper.Map<BattleView>(b));
            return Ok(battleViews);
        }

        [HttpPost("{id}/start")]
        public async Task<ActionResult> StartBattle(int id)
        {
            var battle = await _battleDb.GetBattleById(id);

            if (battle == null)
            {
                return NotFound("could not find battle");
            }


            var teamsWithoutUser = battle.Teams.Where(t => !t.Users.Any());
            if (teamsWithoutUser.Any())
            {
                return BadRequest("one team has no user");
            }

            if (battle.Teams.Count < 2)
            {
                return BadRequest("not enough teams to start battle");
            }


            var actorId = new ActorId($"{id}");
            var proxy = _factory.CreateActorProxy<IBattleActor>(actorId, nameof(BattleActor));
            await proxy.StartBattle(battle);


            await _battleDb.SetBattleToActive(id);


            return Ok();
        }

        [HttpPost("{id}/command/attack")]
        public async Task<ActionResult> ExecuteAttackCommand(int id, [FromBody] AttackCommand command)
        {
            var actorId = new ActorId($"{id}");
            var proxy = _factory.CreateActorProxy<IBattleActor>(actorId, nameof(BattleActor));

            await proxy.ExecuteAttackCommand(command);

            return Ok();
        }

        [HttpPost("{id}/addTeam")]
        public async Task<ActionResult<TeamEntity>> AddTeam(int id, [FromBody] AddTeamRequest request)
        {
            var battle = await _dbBattleDbContext.Battles.FirstOrDefaultAsync(b => b.Id == id);
            if (battle == null)
            {
                return NotFound("could not find battle");
            }

            //todo sanity checks; is mon already in other battle


            var monsterList = request.Monsters.Select(mId => new MonsterEntity
            {
                Id = mId,
            }).ToList();
            var userList = request.Users.Select(u => new UserEntity
            {
                Id = u,
            }).ToList();


            await _dbBattleDbContext.Monsters.AddRangeAsync(monsterList);
            await _dbBattleDbContext.Users.AddRangeAsync(userList);


            var newTeam = new TeamEntity
            {
                Monsters = monsterList, Users = userList,
            };

            var newTeamAdded = await _dbBattleDbContext.Teams.AddAsync(newTeam);

            battle.Teams.Add(newTeamAdded.Entity);

            await _dbBattleDbContext.SaveChangesAsync();


            return Ok(newTeamAdded.Entity);
        }
    }
}
