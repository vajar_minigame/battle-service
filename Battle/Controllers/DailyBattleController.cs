﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Storage;

namespace Service.Battle.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DailyBattleController : ControllerBase
    {
        private readonly IDailyBattleStorage _storage;

        public DailyBattleController(IDailyBattleStorage storage)
        {
            _storage = storage;
        }


        [HttpGet("{userId:int}")]
        public async Task<ActionResult<GetDailyBattlesResponse>> GetDailyBattles([FromRoute] int userId)
        {
            //create daily battles if not already done; reset every night.
            return Ok(await _storage.FindByUserId(userId));
        }
        
        
    }
}
