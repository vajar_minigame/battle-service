﻿using System;

namespace Service.Battle.Storage
{
    public class UnitOfWork : IDisposable
    {
        private readonly BattleDbContext _battleDbContext;

        public UnitOfWork(BattleDbContext battleDbContext)
        {
            _battleDbContext = battleDbContext;
        }

        public void Dispose()
        {
            _battleDbContext.Dispose();
            GC.SuppressFinalize(this);
        }


        public IBattleStorage GetBattleRepo()
        {
            return new BattleStore(_battleDbContext);
        }


        public void Commit()
        {
            _battleDbContext.SaveChanges();
        }
    }
}
