﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Battle.Entities;

namespace Service.Battle.Storage
{
    public interface IBattleStorage
    {
        Task<BattleEntity> Add(BattleEntity battle);

        BattleEntity Update(BattleEntity battle);
        
        Task<BattleEntity?> GetBattleById(int id);
        Task<IEnumerable<BattleEntity>> GetBattlesByUserId(int id);

        Task Remove(int battleId);

        Task SetBattleToActive(int id);

        Task<bool> IsAlreadyInBattle(int id);

        Task AddUserIfNotExists(UserEntity user);

        public Task AddMonsters(IEnumerable<MonsterEntity> monsters);
    }
}
