﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Battle.Entities;

namespace Service.Battle.Storage
{
    public interface IDailyBattleStorage
    {
        /// <summary>
        /// Return all daily battles that were created. There could be still active ones as well
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        Task<List<BattleEntity>>FindByUserId(int userId,bool onlyNew=false);
    }
}
