﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Service.Battle.Entities;
using Service.Battle.Models;

namespace Service.Battle.Storage
{
    public class BattleStore : IBattleStorage
    {
        private readonly BattleDbContext _battleDbContext;

        public BattleStore(BattleDbContext battleDbContext)
        {
            _battleDbContext = battleDbContext;
            _battleDbContext.Database.EnsureCreated();
        }


        public async Task<BattleEntity> Add(BattleEntity battle)
        {
            battle.LastUpdate = DateTime.UtcNow;

            var b = await _battleDbContext.AddAsync(battle);
            return b.Entity;
        }

        public async Task AddMonsters(IEnumerable<MonsterEntity> monsters)
        {
            await _battleDbContext.Monsters.AddRangeAsync(monsters);
        }

        public async Task AddUserIfNotExists(UserEntity user)
        {
            var oldUser = await _battleDbContext.Users.Where(u => u.Id == user.Id).FirstOrDefaultAsync();
            if (oldUser!=null)
            {
                return;
            }

            _battleDbContext.Users.Add(user);


        }


        public BattleEntity Update(BattleEntity battle)
        {
            var b = _battleDbContext.Battles.Update(battle).Entity;
            return b;
        }


        public async Task<BattleEntity?> GetBattleById(int id)
        {
            var battle = await CreateBattleQuery()
                .FirstOrDefaultAsync(b => b.Id == id);

            return battle;
        }

        public async Task Remove(int battleId)
        {
            var battleToRemove = await _battleDbContext.Battles.FirstOrDefaultAsync(b => b.Id == battleId);
            if (battleToRemove != null)
            {
                _battleDbContext.Battles.Remove(battleToRemove);
            }
        }

        public async Task SetBattleToActive(int id)
        {
            var battle = await _battleDbContext.Battles.FindAsync(id);
            battle.State = BattleState.Active;
        }

        public async Task<IEnumerable<BattleEntity>> GetBattlesByUserId(int id)
        {
            return await CreateBattleQuery()
                .Where(b => b.Teams.Any(t => t.Users.Any(u => u.Id == id)))
                .ToListAsync();
        }


        public async Task<bool> IsAlreadyInBattle(int id)
        {
            var battlesQuery = from b in _battleDbContext.Battles
                where b.State == BattleState.Active
                from t in b.Teams
                from mon in t.Monsters
                where mon.Id == id
                select b;

            return await battlesQuery.AnyAsync();
        }


        public IEnumerable<BattleEntity> GetActiveBattles()
        {
            return CreateBattleQuery()
                .Where(b => b.State == BattleState.Active)
                .ToList();
        }


        private IQueryable<BattleEntity> CreateBattleQuery()
        {
            return _battleDbContext.Battles.AsNoTracking()
                .Include(b => b.Teams)
                .ThenInclude(t => t.Monsters)
                .Include(b => b.Teams).ThenInclude(t => t.Users);
        }
    }
}
