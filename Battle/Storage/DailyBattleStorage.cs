﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Service.Battle.Entities;
using Service.Battle.Models;

namespace Service.Battle.Storage
{
    public class DailyBattleStorage : IDailyBattleStorage
    {
        private readonly BattleDbContext _context;

        public DailyBattleStorage(BattleDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Return all daily battles that were created. There could be still active ones as well
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="onlyNew">When true, only returns battles that are not yet started</param>
        /// <returns></returns>
        public async Task<List<BattleEntity>> FindByUserId(int userId, bool onlyNew = true)
        {
            //TODO return empty list when empty
            var query = _context.Users.AsNoTracking()
                .Where(u => u.Id == userId)
                .SelectMany(u => u.TeamEntities)
                .Select(t => t.BattleEntity)
                .Where(e => e.Type == BattleType.Daily);

            if (onlyNew)
            {
                query = query.Where(e => e.State == BattleState.New);
            }

            return await query.ToListAsync();
        }
    }
}
