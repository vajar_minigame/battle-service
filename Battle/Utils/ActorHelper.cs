﻿using Dapr.Actors;
using Dapr.Actors.Client;
using Service.Battle.Actors;

namespace Service.Battle.Utils
{
    public static class ActorHelper
    {
        public static IBattleActor CreateBattleActorProxy(int battleId)
        {
            var actorId = new ActorId($"{battleId}");
            var proxy = ActorProxy.Create<IBattleActor>(actorId, nameof(BattleActor));
            return proxy;
        }
    }
}
