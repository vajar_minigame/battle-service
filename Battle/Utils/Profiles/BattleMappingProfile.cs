﻿using System.Linq;
using AutoMapper;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Models.Commands;
using Service.Battle.Models.Monster;

namespace Service.Battle.Utils.Profiles
{
    public class BattleMappingProfile : Profile
    {
        public BattleMappingProfile()
        {
            CreateMap<int, UserEntity>().ConvertUsing(src => new UserEntity
            {
                Id = src,
            });
            CreateMap<UserEntity, int>().ConvertUsing(src => src.Id);

            CreateMap<User, UserEntity>()
                .ForMember(dest=>dest.TeamEntities,opt=>opt.Ignore());


            CreateMap<int, MonsterEntity>().ConvertUsing(src => new MonsterEntity
            {
                Id = src,
            });

            CreateMap<AddTeamRequest, TeamEntity>().ForMember(dest => dest.Id,
                    opt => opt.Ignore())
                .ForMember(dest => dest.BattleEntity, opt => opt.Ignore());


            CreateMap<Monster, MonsterEntity>().ConvertUsing(src => new MonsterEntity
            {
                Id = src.Id,
            });


            CreateMap<Team, TeamEntity>().ForMember(src => src.Monsters, opt => opt.MapFrom(src => src.Monsters))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.BattleEntity, opt => opt.Ignore());
            CreateMap<Team, TeamView>();
            CreateMap<TeamEntity, TeamView>();
            CreateMap<Monster,int>().ConvertUsing((src,_)=>src.Id);
            CreateMap<MonsterEntity,int>().ConvertUsing((src,_)=>src.Id);
            CreateMap<Models.Battle, BattleView>();
            CreateMap<BattleEntity, BattleView>()
                .ForMember(dest=>dest.Active,opt=>opt.Ignore())
                .ForMember(dest=>dest.Ended,opt=>opt.Ignore());

            CreateMap<Models.Battle, BattleEntity>()
                .ForMember(dest => dest.State, opt => opt.MapFrom((src, _) =>
                {
                    if (src.Active != null)
                    {
                        return BattleState.Active;
                    }

                    if (src.Ended != null)
                    {
                        return BattleState.Ended;
                    }

                    return BattleState.New;
                }))
                .ForMember(dest => dest.Teams, opt => opt.MapFrom(src => src.Teams));
        }
    }
}
