﻿using Microsoft.EntityFrameworkCore;
using Service.Battle.Configurations;
using Service.Battle.Entities;

namespace Service.Battle
{
    public class BattleDbContext : DbContext
    {
        public BattleDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<BattleEntity> Battles { get; set; } = null!;
        public DbSet<TeamEntity> Teams { get; set; } = null!;
        public DbSet<UserEntity> Users { get; set; } = null!;

        public DbSet<MonsterEntity> Monsters { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TeamEntityConfiguration());
            modelBuilder.ApplyConfiguration(new BattleEntityConfiguration());
        }
    }
}
