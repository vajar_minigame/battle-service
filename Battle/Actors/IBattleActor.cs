﻿using System.Threading.Tasks;
using Dapr.Actors;
using Service.Battle.Entities;
using Service.Battle.Models;

namespace Service.Battle.Actors
{
    public interface IBattleActor : IActor
    {
        Task StartBattle(BattleEntity et);

        Task ExecuteAttackCommand(AttackCommand command);

        Task<Models.Battle> GetBattle();
    }
}
