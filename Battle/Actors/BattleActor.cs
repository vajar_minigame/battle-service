﻿using System.Linq;
using System.Threading.Tasks;
using Dapr.Actors.Runtime;
using Microsoft.Extensions.Logging;
using Service.Battle.Entities;
using Service.Battle.Models;
using Service.Battle.Services;
using Service.Battle.Storage;

namespace Service.Battle.Actors
{
    public class BattleActor : Actor, IBattleActor
    {
        private const string StateName = "battles";
        private readonly IBattleService _battleService;
        private readonly ILogger<BattleActor> _logger;
        private readonly IMonsterClient _monsterClient;


        public BattleActor(ActorHost host,
            ILogger<BattleActor> logger,
            IMonsterClient monsterClient,
            IBattleService battleService) : base(host)
        {
            _logger = logger;
            _monsterClient = monsterClient;
            _battleService = battleService;

            _logger.LogInformation("this really happens!!");
        }


        public async Task StartBattle(BattleEntity battleEntity)
        {
            var monstersTasks = battleEntity.Teams.SelectMany(t => t.Monsters)
                .Select(async m => await _monsterClient.GetMonById(m.Id));

            var maybeMonsters = (await Task.WhenAll(monstersTasks)).ToList();
            var monsters = maybeMonsters.Where(m => m != null).Select(m => m!).ToList();

            var battle = Models.Battle.FromEntity(battleEntity, monsters);

            battle = await _battleService.StartBattle(battle);
            await StateManager.AddStateAsync(StateName, battle);
        }


        public async Task ExecuteAttackCommand(AttackCommand command)
        {
            var battle = await StateManager.GetStateAsync<Models.Battle>(StateName);
            battle = await _battleService.ExecuteAttackCommand(battle, command);
            await StateManager.SetStateAsync(StateName, battle);

            await StateManager.SaveStateAsync();
        }

        public async Task<Models.Battle> GetBattle()
        {
            var battle = await StateManager.GetStateAsync<Models.Battle>(StateName);
            return battle;
        }

        // This method is called whenever an actor is activated.
        // An actor is activated the first time any of its methods are invoked.
        protected override Task OnActivateAsync()
        {
            _logger.LogInformation("actor is started yo");
            // Provides opportunity to perform some optional setup.
            return Task.CompletedTask;
        }
    }
}
