﻿namespace Service.Battle.Models.Monster
{
    public record BattleValues
    {
        public int Attack { get; init; }
        public int Defense { get; init; }
        public int MaxHp { get; init; }
        public int RemainingHp { get; init; }
    }
}
