﻿namespace Service.Battle.Models.Monster
{
    public class BodyValues
    {
        public float RemainingSaturation { get; set; }
        public int MaxSaturation { get; set; }
        public int Mass { get; set; }
    }
}
