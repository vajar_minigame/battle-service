﻿using System;

namespace Service.Battle.Models.Monster
{
    public class Monster
    {
        public int Id { get; init; }

        public string Name { get; set; } = "";

        public int UserId { get; set; }

        public BattleValues BattleValues { get; set; } = null!;

        public StatusValues StatusValues { get; set; } = null!;

        public DateTime LastUpdate { get; set; }

        public bool IsTest { get; set; }
    }
}
