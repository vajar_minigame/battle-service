﻿using System.ComponentModel.DataAnnotations;

namespace Service.Battle.Models
{
    public class BattleQueryRequest
    {
        [Required]
        public int? UserId { get; set; }
    }
}
