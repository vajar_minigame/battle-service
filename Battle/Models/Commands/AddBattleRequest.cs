﻿using System.Collections.Generic;

namespace Service.Battle.Models.Commands
{
    public record AddBattleRequest
    {
        public ICollection<AddTeamRequest> Teams { get; init; } = new List<AddTeamRequest>();

        public bool IsTest { get; init; }
    }
}
