﻿using System.Collections.Generic;

namespace Service.Battle.Models.Commands
{
    public class AddTeamRequest
    {
        public IEnumerable<int> Users { get; set; } = new List<int>();
        public IEnumerable<int> Monsters { get; set; } = new List<int>();
    }
}
