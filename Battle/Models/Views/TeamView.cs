﻿using System.Collections.Generic;

namespace Service.Battle.Models
{
    public class TeamView
    {
        public int Id { get; set; }

        public IEnumerable<int> Users { get; set; } = new List<int>();

        public virtual IEnumerable<int> Monsters { get; set; } = new List<int>();
    }
}
