﻿using System;
using System.Collections.Generic;
namespace Service.Battle.Models
{
    public class BattleView
    {
        public int Id { get; set; }
        public IEnumerable<TeamView> Teams { get; init; } = new List<TeamView>();
        public DateTime LastUpdate { get; set; }
        public ActiveBattle? Active { get; set; }
        public BattleEnded? Ended { get; set; }
        public bool IsTest { get; set; }
    }
}
