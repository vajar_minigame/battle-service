﻿using System.Collections.Generic;
using System.Linq;
using Service.Battle.Entities;

namespace Service.Battle.Models
{
    public class Team
    {
        public int Id { get; set; }

        public IEnumerable<int> Users { get; set; } = new List<int>();

        public virtual IEnumerable<Monster.Monster> Monsters { get; set; } = new List<Monster.Monster>();


        public static Team FromEntitiy(TeamEntity te, List<Monster.Monster> mons)
        {
            return new()
            {
                Id = te.Id,
                Users = te.Users.Select(u => u.Id),
                Monsters = te.Monsters.Select(me => mons.First(m => m.Id == me.Id)),
            };
        }
    }
}
