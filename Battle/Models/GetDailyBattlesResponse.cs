﻿using System;
using System.Collections.Generic;

namespace Service.Battle.Models
{
    public record GetDailyBattlesResponse
    {
        public DateTime LastReset { get; init; }
        public List<int> BattleIds { get; init; } = new List<int>();
    }
}
