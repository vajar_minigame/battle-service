﻿using System.Text.Json.Serialization;

namespace Service.Battle.Models
{
    public abstract record BattleEventBase
    {
        public int BattleId { get; init; }
        public abstract BattleEventType EventType { get; init; }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BattleEventType
    {
        BattleAdded,
        TurnQueue,
        BattleStarted,
        BattleEnded,
    }
}
