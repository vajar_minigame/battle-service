namespace Service.Battle.Models
{
    public class User
    {
        public string Username { get; set; }
        public int Id { get; set; }
    }
}
