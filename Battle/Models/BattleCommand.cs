﻿using System.ComponentModel.DataAnnotations;

namespace Service.Battle.Models
{
    public interface IBattleCommand
    {
    }

    public class AttackCommand : IBattleCommand
    {
        [Required]
        public int Source { get; set; }

        [Required]
        public int Target { get; set; }
    }
}
