﻿using System;
using System.Collections.Generic;

namespace Service.Battle.Models
{
    public record ActiveBattle
    {
        public int Id { get; init; }
        public DateTime StartTime { get; init; }
        public int TurnCount { get; set; }
        public List<int> TurnQueue { get; set; } = new();
        public DateTime LastAction { get; set; }
    }
}
