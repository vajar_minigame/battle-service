﻿using Service.Battle.Entities;

namespace Service.Battle.Models
{
    public record BattleEndedEvent : BattleEventBase
    {
        public Battle Battle { get; init; } = null!;
        public TeamEntity Winner { get; init; } = null!;

        public override BattleEventType EventType { get; init; } = BattleEventType.BattleEnded;
    }
}
