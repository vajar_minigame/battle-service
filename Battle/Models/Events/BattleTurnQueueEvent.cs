﻿namespace Service.Battle.Models
{
    public record BattleTurnQueueEvent : BattleEventBase
    {
        public Battle Battle { get; init; } = null!;

        public override BattleEventType EventType { get; init; } = BattleEventType.TurnQueue;
    }
}
