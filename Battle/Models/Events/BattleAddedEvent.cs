﻿namespace Service.Battle.Models
{
    public record BattleAddedEventBase : BattleEventBase
    {
        public Battle Battle { get; set; } = null!;
        public override BattleEventType EventType { get; init; } = BattleEventType.BattleAdded;
    }
}
