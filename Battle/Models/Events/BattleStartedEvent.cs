﻿namespace Service.Battle.Models
{
    public record BattleStartedEvent : BattleEventBase
    {
        public Battle Battle { get; init; } = null!;

        public override BattleEventType EventType { get; init; } = BattleEventType.BattleStarted;
    }
}
