﻿using System;
using System.Collections.Generic;
using System.Linq;
using Service.Battle.Entities;

namespace Service.Battle.Models
{
    public record Battle
    {
        public int Id { get; set; }
        public IEnumerable<Team> Teams { get; init; } = new List<Team>();
        public DateTime LastUpdate { get; set; }
        
        public BattleType Type { get; init; }
        
        public ActiveBattle? Active { get; set; }
        public BattleEnded? Ended { get; set; }
        public bool IsTest { get; set; }


        public static Battle FromEntity(BattleEntity be, List<Monster.Monster> monsters)
        {
            return new()
            {
                Teams = be.Teams.Select(t => Team.FromEntitiy(t, monsters)),
                Id = be.Id,
                IsTest = be.IsTest,
                LastUpdate = be.LastUpdate,
            };
        }
    }


    public enum BattleState
    {
        New,
        Active,
        Ended,
    }

    public record TurnQueueElement
    {
        public int Id { get; set; }
        public MonsterEntity Monster { get; set; } = null!;
        public int Priority { get; set; }
    }


    public record BattleEnded
    {
        public int Id { get; set; }
        public string Report { get; set; } = null!;
        public Team Winner { get; set; } = null!;
    }
}
